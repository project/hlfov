
// Implement confirmation windows for hlfov unflag links.
Drupal.behaviors.hlfovModalConfirmation = function(context) {
  /**
   * This is a copy of flagClick() in Drupal.flagLink() in flag.js.
   * @todo Patch flag.js, so that flagClick() may be invoked as an API function in flag.js.
   */
  function flagClick() {
    // 'this' won't point to the element when it's inside the ajax closures,
    // so we reference it using a variable.
    var element = this;

    // While waiting for a server response, the wrapper will have a
    // 'flag-waiting' class. Themers are thus able to style the link
    // differently, e.g., by displaying a throbber.
    var $wrapper = $(element).parents('.flag-wrapper');
    if ($wrapper.is('.flag-waiting')) {
      // Guard against double-clicks.
      return false;
    }
    $wrapper.addClass('flag-waiting');

    // Hide any other active messages.
    $('span.flag-message:visible').fadeOut();

    // Send POST request
    $.ajax({
      type: 'POST',
      url: element.href,
      data: { js: true },
      dataType: 'json',
      success: function (data) {
        if (data.status) {
          // Success.
          data.link = $wrapper.get(0);
          $.event.trigger('flagGlobalBeforeLinkUpdate', [data]);
          if (!data.preventDefault) { // A handler may cancel updating the link.
            data.link = updateLink(element, data.newLink);
          }
          $.event.trigger('flagGlobalAfterLinkUpdate', [data]);
        }
        else {
          // Failure.
          alert(data.errorMessage);
          $wrapper.removeClass('flag-waiting');
        }
      },
      error: function (xmlhttp) {
        alert('An HTTP error '+ xmlhttp.status +' occurred.\n'+ element.href);
        $wrapper.removeClass('flag-waiting');
      }
    });
    return false;
  }

  /**
   * A click handler for hlfov (un)flag links.
   */
  function hlfovClick(event) {
    // Don't follow the link, regardless of the confirmation dialogue.
    event.preventDefault();

    // Get the row, so we can access it easily.
    var viewsRow = $(this).parents('.views-row:first');

    // Guard against double clicks; Check we aren't already waiting for the
    // confirmation dialogue to return.
    if (viewsRow.hasClass('hlfov-confirming')) {
      return false;
    }
    viewsRow.addClass('hlfov-confirming');

    var confirmationLink = ($(this).attr('href').indexOf('/confirm/') > 0);
    // The confirmation dialogue message uses the confirmation link's
    // title="" attribute.
    // @todo Make the confirmation message more flexible & meaningful.
    // @todo Remove other punctuation chars ("[.!?:;%]") from the end of the
    // message before appending the question mark character '?'.
    var confirmationQuestion = $(this).attr('title') + '?';

    // @todo Use a modal dialogue library/API if one is available.  E.g.
    // jQuery UI, modalFrames, automodal, lightbox etc.
    if (!confirmationLink || confirm(confirmationQuestion)) {
      // Turn the .views-row into a throbber.
      viewsRow.hlfovThrobify();

      // The URI for the AJAX API does not have /confirm/ in it.
      this.href = this.href.replace('/confirm/', '/');

      // Call Flag's AJAX API.
      flagClick.call(this);
    }
    else {
      viewsRow.removeClass('hlfov-confirming');
    }

    // Don't follow the link, regardless of the confirmation dialogue.
    return false;
  }

  // Bind to the click event of all hlfov (un)flag links.
  $('.flag-link-hlfov', context).click(hlfovClick);
  $('.flag-link-hlfov-confirm', context).click(hlfovClick);
};

// Binds to flag's flagGlobalBeforeLinkUpdate event.
$(document).bind('flagGlobalBeforeLinkUpdate', function(event, data) {
  // Check if this is a hlfov link or not.
  var $flagLink = $('a.flag', data.link);
  if ($flagLink.hasClass('flag-link-hlfov') || $flagLink.hasClass('flag-link-hlfov-confirm')) {
    // Prevent flagClick() from unnecessary DOM manipulation.
    data.preventDefault = true;

    // Get the view we are working with.
    var $view = $(data.link).parents('.view:first');

    // Get the row we need to remove.
    var $row = $(data.link).parents('.views-row:first');

    // Save the parent too.
    var $rowParent = $row.parent();

    // Store the class and the reference to the next row.
    var rowClass = $row.attr('class');
    var $next = $row.next();

    // Remove the row we don't want anymore.
    $row.remove();

    // Bump all of the classes along one row.
    while ($next.length) {
      var tempClass = $next.attr('class');
      $next.attr('class', rowClass);
      rowClass = tempClass;
      $next = $next.next();
    }

    // Retrieve a new item from the server with an AJAX callback.
    if ($('.pager', $view).length) {
      var matches = $view.attr('class').match(/view-dom-id-([0-9]+)/);

      if (matches.length > 1) {
        var viewsDomId = Number(matches[1]);

        // @todo Add support for non-ajax views.
        for (var i in Drupal.settings.views.ajaxViews) {
          if (Drupal.settings.views.ajaxViews[i].view_dom_id === viewsDomId) {
            break;
          }
        }

        var params = Drupal.settings.views.ajaxViews[i];

        $.getJSON(Drupal.settings.basePath + 'hlfov/ajax/item', params, function(data) {
          var $newRow = $(data.display).find('.views-row');
          $rowParent.append($newRow);
          $newRow.hide().attr('class', rowClass).removeClass('hlfov-confirming');
          Drupal.attachBehaviors($newRow);
          $newRow.fadeIn();

          if ($rowParent.children().length) {
            // Restore the 'last' class.
            $rowParent.children('.views-row:last-child').addClass('views-row-last');
          }
          else {
            // @todo Set empty text, as returned by AJAX callback.
          }
        });
      }
    }
  }
});

// Provide anonymous scope to attach a plugin to jQuery.
(function($) {
  // A simple jQuery plugin to subtly turn elements into throbbers.
  $.fn.hlfovThrobify = function(event, options) {
    // "this" is an array of DOM elements to apply the plugin to.
    this.each(function() {
      // Theme markup for a throbber.  Leave all appearance to hlfov.css.
      var $throbber = $('<div class="hlfov-throbber"><div class="inner"></div></div>');
      $throbber.width($(this).width());
      $throbber.height($(this).height());

      // Hide all the children of the element, and append the throbber.
      // This allows us to unThrobify if we really need to.
      $(this).children().hide()
      $(this).append($throbber);
    });
  }
})(jQuery);
